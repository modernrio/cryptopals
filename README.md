Cryptopals crypto challenges
----------------------------
These are my solutions for the crypto challenges.
All challenge descriptions and files (except the solutions) belong to Cryptopals and
are copied here for reference.

Source: https://cryptopals.com/
